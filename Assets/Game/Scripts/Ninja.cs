﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ninja : MonoBehaviour
{
	public GraphicRaycaster m_Raycaster;
	public PointerEventData m_PointerEventData;
	public EventSystem m_EventSystem;

	public GameObject projectilePrefab;

	private Animator animator;
	private Rigidbody2D rb;
	private Coroutine fireCoroutine;
	public float acceleration;
	public float speed;
	private bool attemptShot;
	private bool hadShot;

	public Image up;
	public Image down;
	public Image left;
	public Image right;
	public Image swipeArea;

	void Start()
	{
		animator = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
	}
	void Update()
	{
		animator.SetBool("moving", true);
#if UNITY_ANDROID && !UNITY_EDITOR
		if (Input.touchCount > 0) {
				m_PointerEventData = new PointerEventData(m_EventSystem);
				m_PointerEventData.position = Input.touches[0].position;
				List<RaycastResult> results = new List<RaycastResult>();
				m_Raycaster.Raycast(m_PointerEventData, results);

				if (results.Count > 0) {
					if (results.Select(t => t.gameObject).Contains(up.gameObject)) {
						MoveUpStart();
					}
					if (results.Select(t => t.gameObject).Contains(down.gameObject)) {
						MoveDownStart();
					}
					if (results.Select(t => t.gameObject).Contains(left.gameObject)) {
						MoveLeftStart();
					}
					if (results.Select(t => t.gameObject).Contains(right.gameObject)) {
						MoveRightStart();
					}
					if (!attemptShot && !hadShot && results.Select(t => t.gameObject).Contains(swipeArea.gameObject)) {
						fireCoroutine = StartCoroutine(StartFireSwipe(Input.touches[0].position));
					}
				} else {
					rb.velocity = Vector2.zero;
				}
		} else {
				MoveUpStop();
				MoveDownStop();
				MoveLeftStop();
				MoveRightStop();
			StopCoroutine(fireCoroutine);
			hadShot = false;
			attemptShot = false;
		}
#endif
#if UNITY_EDITOR
		if (Input.GetMouseButton(0)) {
			m_PointerEventData = new PointerEventData(m_EventSystem);
			m_PointerEventData.position = Input.mousePosition;
			List<RaycastResult> results = new List<RaycastResult>();
			m_Raycaster.Raycast(m_PointerEventData, results);

			if (results.Count > 0) {
				if (results.Select(t => t.gameObject).Contains(up.gameObject)) {
					MoveUpStart();
				}
				if (results.Select(t => t.gameObject).Contains(down.gameObject)) {
					MoveDownStart();
				}
				if (results.Select(t => t.gameObject).Contains(left.gameObject)) {
					MoveLeftStart();
				}
				if (results.Select(t => t.gameObject).Contains(right.gameObject)) {
					MoveRightStart();
				}
				if (!attemptShot && !hadShot && results.Select(t => t.gameObject).Contains(swipeArea.gameObject)) {
					fireCoroutine = StartCoroutine(StartFireSwipe(Input.mousePosition));
				}
			} else {
				rb.velocity = Vector2.zero;
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			m_PointerEventData = new PointerEventData(m_EventSystem);
			m_PointerEventData.position = Input.mousePosition;
			List<RaycastResult> results = new List<RaycastResult>();
			m_Raycaster.Raycast(m_PointerEventData, results);

			if (results.Select(t => t.gameObject).Contains(up.gameObject)) {
				MoveUpStop();
			}
			if (results.Select(t => t.gameObject).Contains(down.gameObject)) {
				MoveDownStop();
			}
			if (results.Select(t => t.gameObject).Contains(left.gameObject)) {
				MoveLeftStop();
			}
			if (results.Select(t => t.gameObject).Contains(right.gameObject)) {
				MoveRightStop();
			}
			if (fireCoroutine != null) {
				StopCoroutine(fireCoroutine);
			}
			hadShot = false;
			attemptShot = false;
		}
#endif
		if (Input.GetKey(KeyCode.D)) {
			MoveRightStart();
		}
		if (Input.GetKeyUp(KeyCode.D)) {
			MoveRightStop();
		}
		if (Input.GetKey(KeyCode.A)) {
			MoveLeftStart();
		}
		if (Input.GetKeyUp(KeyCode.A)) {
			MoveLeftStop();
		}
		if (Input.GetKey(KeyCode.W)) {
			MoveUpStart();
		}
		if (Input.GetKeyUp(KeyCode.W)) {
			MoveUpStop();
		}
		if (Input.GetKey(KeyCode.S)) {
			MoveDownStart();
		}
		if (Input.GetKeyUp(KeyCode.S)) {
			MoveDownStop();
		}
		if (Mathf.Abs(rb.velocity.magnitude) <= 0.1f) {
			rb.velocity = Vector2.zero;
			animator.SetBool("moving", false);
		}
		animator.SetFloat("veloX", rb.velocity.x);
		animator.SetFloat("veloY", rb.velocity.y);
	}
	public void Die()
	{
		Vector3 spawnPosition = GameObject.FindObjectOfType<PlayerSpawnPosition>().transform.position;
		transform.position = new Vector3(spawnPosition.x, spawnPosition.y, transform.position.z);
	}

	private IEnumerator StartFireSwipe(Vector3 startPosition)
	{
		attemptShot = true;
		yield return new WaitForSeconds(0.2f);
#if UNITY_EDITOR
		Vector3 endPosition = Input.mousePosition;
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
		if (Input.touchCount <= 0) {
			hadShot = false;
			attemptShot = false;
			yield return null;
		}
		Vector3 endPosition = Input.touches[0].position;
#endif
		Vector3 relativePosition = startPosition - endPosition;
		float threshHold = 70;
		if (relativePosition.magnitude > threshHold) {
			GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity, transform);
			float velocity = 5;

			if (relativePosition.y < -threshHold) {
				projectile.transform.position += new Vector3(0, 1);
				projectile.GetComponent<Rigidbody2D>().velocity = new Vector3(0, velocity, 0);
				//Debug.Log("Shoot up");
			} else if (relativePosition.y > threshHold) {
				projectile.transform.position += new Vector3(0, -1);
				projectile.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -velocity, 0);
				//Debug.Log("Shoot down");
			} else if (relativePosition.x < -threshHold) {
				projectile.transform.position += new Vector3(1, 0);
				projectile.GetComponent<Rigidbody2D>().velocity = new Vector3(velocity, 0, 0);
				//Debug.Log("Shoot right");
			} else if (relativePosition.x > threshHold) {
				projectile.transform.position += new Vector3(-1, 0);
				projectile.GetComponent<Rigidbody2D>().velocity = new Vector3(-velocity, 0, 0);
				//Debug.Log("Shoot left");
			}
			hadShot = true;
		}
	}

	private void MoveUpStart()
	{
		if (rb.velocity.y < speed) {
			rb.velocity = rb.velocity + new Vector2(0, acceleration);
		}
	}
	private void MoveUpStop()
	{
		rb.velocity = rb.velocity + new Vector2(0, -rb.velocity.y);
	}
	private void MoveDownStart()
	{
		if (rb.velocity.y > -speed) {
			rb.velocity = rb.velocity + new Vector2(0, -acceleration);
		}
	}
	private void MoveDownStop()
	{
		rb.velocity = rb.velocity + new Vector2(0, -rb.velocity.y);
	}
	private void MoveLeftStart()
	{
		if (rb.velocity.x > -speed) {
			rb.velocity = rb.velocity + new Vector2(-acceleration, 0);
		}
	}
	private void MoveLeftStop()
	{
		rb.velocity = rb.velocity + new Vector2(-rb.velocity.x, 0);
	}
	private void MoveRightStart()
	{
		if (rb.velocity.x < speed) {
			rb.velocity = rb.velocity + new Vector2(acceleration, 0);
		}
	}
	private void MoveRightStop()
	{
		rb.velocity = rb.velocity + new Vector2(-rb.velocity.x, 0);
	}
}
