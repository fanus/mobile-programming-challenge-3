﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	private HUD hud;

    private void Start()
    {
        hud = GameObject.FindObjectOfType<HUD>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
		hud.AddCoin();
        Destroy(gameObject);
    }
}
