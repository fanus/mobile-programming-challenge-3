﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSystem : MonoBehaviour
{
    public GameObject help;
    public GameObject main;
    public int sceneIndex;

    void Start()
    {
		ShowMain();
    }

    public void ShowMain()
    {
        main.SetActive(true);
        help.SetActive(false);
    }

    public void ShowHelp()
    {
        main.SetActive(false);
        help.SetActive(true);
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
