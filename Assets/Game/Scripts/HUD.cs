﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Text coins;
    private int coinTotal;
    private int coinCount = 0;
    public Text time;
    private DateTime startTime;

    void Start()
    {
        startTime = DateTime.Now;
        coinTotal = GameObject.FindObjectsOfType<Coin>().Length;

        coins.text = String.Format("COINS: {0} of {1}", coinCount, coinTotal.ToString());
        time.text = String.Format("TIME: {0} seconds", (DateTime.Now - startTime).TotalSeconds);
    }

    void Update()
    {
        if(coinCount != coinTotal)
        {
            time.text = String.Format("TIME: {0} seconds", Math.Round((DateTime.Now - startTime).TotalSeconds));
        }
    }


    public void AddCoin()
    {
        coinCount++;
        coins.text = String.Format("COINS: {0} of {1}", coinCount, coinTotal.ToString());
    }
}
