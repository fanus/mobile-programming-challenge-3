﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    private Flame flame;
    private Coroutine trapRoutine;
    private AudioSource audioSource;
    void Start()
    {
        flame = GetComponentInChildren<Flame>();
        audioSource = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        trapRoutine = StartCoroutine(SpringTrap(collider));
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        StopCoroutine(trapRoutine);
    }
    private IEnumerator SpringTrap(Collider2D collider)
    {
        audioSource.Play();
        yield return new WaitForSeconds(0.5f);
        flame.Fire();
        yield return new WaitForSeconds(0.5f);
        collider.attachedRigidbody.GetComponent<Ninja>().Die();
    }
}
